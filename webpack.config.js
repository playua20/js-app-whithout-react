const path = require('path'); // установлен по стандорту с node.js

const MiniCssExtractPlugin = require('mini-css-extract-plugin'); // для build
// const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin'); // устаревший, для webpack 4
const cssMinimizerWebpackPlugin = require('css-minimizer-webpack-plugin'); // для разработки
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

// var config = {
//     entry: './src/index.js',
//     //...
//   };

// module.exports = (env, argv) => {
module.exports = {
    entry: './src/index.js',
    output: {
        // для build
        path: path.resolve(__dirname, './build'),
        filename: 'js/bundle.min.js',
        publicPath: '/build/'
    },
    devtool: 'inline-source-map', // для обработки ошибок в файлах в консоли браузера
    devServer: {
        port: 7000,
        compress: false,
        open: true,
        hot: true
        // overlay: true
    },
    // devServer: {
    //     // overlay: true,
    //     open: true
    // },
    module: {
        rules: [
            {
                test: /\.(s[ac]ss|css)$/,
                use: [
                    MiniCssExtractPlugin.loader, // для build. Создаем минимизированный файт css
                    // 'style-loader', // для dev. Создает тег <style> на странице, помещает туда стили. Исп в реж. разработки, т.к. инлайн стили быстрее создаются, чем генерация css-файла
                    // { loader: 'style-loader', options: { injectType: 'styleTag' } }, // для dev
                    'css-loader', // код css помещаем в js файл
                    'postcss-loader', // вендоные префиксы. Для build
                    'sass-loader' // scss в css
                ]
            },
            {
                test: /\.js?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                    //, options: {
                    //     presets: ['@babel/preset-env']
                    // }
                }
            },
            {
                test: /\.(jpe?g|gif|png|svg)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[hash].[ext]',
                    outputPath: 'images'
                }
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: './src/template.html'
        }),
        new MiniCssExtractPlugin({
            // для build
            filename: './css/bundle.min.css'
        })
        // new cssMinimizerWebpackPlugin() // для dev
    ]
};
