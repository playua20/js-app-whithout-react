import cn from 'classnames';

import { Component } from '../../core/Component';
import './Input.scss';

const DEFAULT_TYPE = 'text';
const DEFAULT_AUTOCOMPLATE = 'OFF';

export class Input extends Component {
    constructor(options = {}) {
        const {
            className,
            type = DEFAULT_TYPE,
            autocomplate = DEFAULT_AUTOCOMPLATE,
            ...other
        } = options;
        super({
            tagName: 'input',
            className: cn('input', className),
            attrs: { type, autocomplate, ...other }
        });
    }
}
