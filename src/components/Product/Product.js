//  Класс, наследуется от Component

import { Component } from '../../core/Component';
import { Button } from '../Button/Button';
import { Rating } from '../Rating/Rating';
import { Loader } from '../Loader/Loader';
import './Product.scss';

const MAX_CHARS_AMOUNT = 150;

export class Product extends Component {
    constructor(options = {}) {
        const {
            id,
            category, // h2
            model, // h3
            manufacturer, // h2
            country, // h4
            imageSrc,
            price, //span
            rating, // будет отдельным компонентом
            description, // p
            warranty // span
        } = options;

        const stars = new Rating({ value: rating });
        const shortDescription = description.slice(0, MAX_CHARS_AMOUNT);

        const loader = new Loader();

        const wishListButton = new Button({
            className: 'button--primary product__button product__button--wishlist',
            title: 'Add to Wishlist',
            children: '<i class="fas fa-heart product__icon"></i>'
        });

        const cartButton = new Button({
            className: 'button--primary product__button product__button--cart',
            title: 'Add to Cart',
            children: '<i class="fas fa-shopping-bag product__icon"></i>'
        });

        super({
            className: 'product',
            attrs: {
                'data-id': id
            },
            children: `
            <div class="product__header">
                <h2 class="product__category">${category}</h2>
                ${stars.toHtml()}
            </div>
            
            <div class="product__main">
                <div class="product__image-wrapper">
                    <img class="product__image product__image--hidden" src="${imageSrc}" alt="${category}">
                    ${loader.toHtml()}
                </div>

                <div class="product__info">
                    <h2 class="product__manufacturer">${manufacturer}</h2>
                    <h3 class="product__model">${model}</h3>
                    <h4 class="product__country">${country}</h4>
                    <span class="product__warranty">Warranty: ${warranty}</span>
                    <p class="product__description">${shortDescription}</p>
                </div>

                <div class="product__footer">
                <div class="product__price-wrapper">
                    <span class="product__label">Price</span>

                    <div>
                        <span class="product__price">${price}</span>
                        <span class="product__currency">USD</span>
                    </div>
                </div>
                <div class="product__actions">
                    ${wishListButton.toHtml()}
                    ${cartButton.toHtml()}
                </div>

            </div>
            `
        });

        this._image = this._component.querySelector('.product__image');

        this._image.addEventListener('load', this.handleImageLoad.bind(this));
    }

    handleImageLoad() {
        setTimeout(() => {
            const loader = this._image.nextElementSibling;

            loader.remove();
            this._image.classList.remove('product__image--hidden');
        }, 300);
    }
}
