// точка входа в наше приложение

import { App } from './components/App/App'; // импорт готового приложения
import { GlobalState } from './core/GlobalState'; // импорт готового приложения
import { render } from './core/render'; // рендерим приложение на странице
import products from './assets/database/products.json';
import './styles/index.scss';

const INITIAL_CATEGORY = 'TV';
const INITIAL_PAGE = 1;

const gs = new GlobalState({
    activeCategory: INITIAL_CATEGORY,
    activePage: INITIAL_PAGE,
    cart: []
});

const options = {
    gs, // динамические значения глобального стейта
    categories: [...new Set(products.map(p => p.category))], // статические значения глобал стейта
    products
};

const app = new App(options);

render(app, document.getElementById('app'));

// const productList = document.querySelector('.product-list');

// const filteredProducts = products.filter(p => p.category === activeCategory);

// productList.innerHTML = '';
// productList.append(...filteredProducts.map(p => p.toNode()));
